## casadi_vendor (foxy) - 3.5.1-1

The packages in the `casadi_vendor` repository were released into the `foxy` distro by running `/usr/bin/bloom-release --ros-distro foxy --track foxy casadi_vendor` on `Wed, 19 Aug 2020 03:44:36 -0000`

The `casadi_vendor` package was released.

Version of package(s) in repository `casadi_vendor`:

- upstream repository: https://gitlab.com/autowarefoundation/autoware.auto/casadi_vendor.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `3.5.1-1`

Versions of tools used:

- bloom version: `0.9.7`
- catkin_pkg version: `0.4.22`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.2`
- vcstools version: `0.1.42`


## casadi_vendor (dashing) - 3.5.1-1

The packages in the `casadi_vendor` repository were released into the `dashing` distro by running `/usr/bin/bloom-release --track dashing --rosdistro dashing casadi_vendor` on `Tue, 25 Feb 2020 23:10:51 -0000`

The `casadi_vendor` package was released.

Version of package(s) in repository `casadi_vendor`:

- upstream repository: https://gitlab.com/autowarefoundation/autoware.auto/casadi_vendor.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `3.5.1-1`

Versions of tools used:

- bloom version: `0.9.1`
- catkin_pkg version: `0.4.12`
- rosdep version: `0.18.0`
- rosdistro version: `0.8.0`
- vcstools version: `0.1.42`


# casadi_vendor-release

Release repository for the CASADI vendor package